# RESPOSTAS QUESTÕES PRATICAS

3. Para usar o código no terminal basta usar o comando ```node calculadora.js a b``` sendo a e b números. O programa funciona pegando tudo que é escrito na linha de comando, ignorando os dois primeiros argumentos, e soma como como uma calculadora as partes inteiras dos números fornecidos.
4. Adição de calculadora.js
5. Essa mudança cria um metódo que é uma função que deixa o código mais claro.
6. ```process.argv.slice(2);``` remove os dois primeiros argumentos, então ```parseInt(args[0])``` e ```parseInt(args[1])``` receberiam soma/sub e o primeiro elemento da soma/subtração e não o primeiro e o segundo elemento da soma/subtração. Além disso no final estava ```arg``` e não ```args```.
7. Criação de uma nova branch e divisão.
8. Merge das branchs

9.  Adição da linha ```const args = process.argv.slice(2);```.